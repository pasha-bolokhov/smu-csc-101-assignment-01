# Assignment 1

All solutions must be placed on [CodeAnywhere](https://codeanywhere.com) into the folder `a01`.
Put each problem into the corresponding file `p1.py`, `p2.py`, `p3.py` *etc*.

All files must produce *no errors* when invoked with Python *e.g.* as
```
python p1.py
```




### Problem 1

Create a python function `rectangle_area()` to compute the area of a rectangle based on the length and width of the rectangle




### Problem 2

Create a python function `join_back()` which takes two string arguments `s1` and `s2`, and concatenates strings in the opposite order.
That is, if you invoke it as `join_back('Hello', 'world')` it should produce `'worldHello'`:
```python
>>>join_back('Hello', 'world')
worldHello
```




### Problem 3

You and two of your friends went to a restaurant and each spent `amount1`, `amount2` and `amount3` towards your meals.
Now you want to calculate the 15% tip you were going to add for this dinner.
Create a python function `restaurant_tip()` which takes three arguments `amount1`, `amount2` and `amount3`. The function should return
the 15% tip of the sum of all three amounts
